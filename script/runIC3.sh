#!/usr/bin/env bash
# sample test case sh ./runIC3.sh /Users/tangchris/project/EdoDroid/testapp/test.apk

mysql -uroot -ptoor -e 'drop database cc; create database cc';
mysql -uroot -ptoor cc < schema;

ANDROID_JAR=android.jar

APK=$1

appName=`basename $APK .apk`

APK_FOLDER=${APK%/*}

sh ./runDare.sh $APK

retargetedPath=$APK_FOLDER/retargeted


java -Xmx8192m -jar ic3-0.2.0-full.jar  -apkormanifest $APK --input $retargetedPath/$appName -cp $ANDROID_JAR -db ./cc.properties


# clone the output to target and remove all temp folder
rm -rf $APK_FOLDER/optimized
rm -rf $APK_FOLDER/optimized-decompiled
rm -rf $APK_FOLDER/retargeted
rm -rf $APK_FOLDER/stats.csv