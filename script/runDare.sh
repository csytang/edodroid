#!/usr/bin/env bash
# sample test case ./runDare.sh /Users/tangchris/project/EdoDroid/test/test.apk

APK=$1
APK_FOLDER=${APK%/*}

# run Dare
echo Running Dare to build the retargeted

../dare-1.1.0-macos/dare -d $APK_FOLDER $APK

echo Finish Dare the retargeted
# clone the output to target and remove all temp folder