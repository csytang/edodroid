package hk.polyu.comp.edo.db;

public enum ConnectionType {
    C3P0,
    JNDI,
    JDBC;
}
