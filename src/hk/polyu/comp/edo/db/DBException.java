package hk.polyu.comp.edo.db;

public class DBException extends Exception {

    private static final long serialVersionUID = -4206624164356825523L;

    public DBException() {
    }

    public DBException(String message) {
        super(message);
    }

    public DBException(Throwable cause) {
        super(cause);
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

}
