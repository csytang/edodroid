package hk.polyu.comp.edo.target.dependency;

import hk.polyu.comp.edo.target.*;
import hk.polyu.comp.edo.target.event.Event;
import hk.polyu.comp.edo.target.event.SupportingEvent;
import hk.polyu.comp.edo.target.traversal.CallGraphTraversal;

import java.util.List;

abstract class DependencyResolver<T extends Dependence> {
    public abstract List<CallGraphTraversal.Plugin> getCallGraphPlugins();

    public abstract void computeEventDependencies(Event event);
    public abstract SupportingEvent resolveDependence(Event event, T dependence);
}
