package hk.polyu.comp.edo.target.traversal;

import soot.MethodOrMethodContext;
import soot.SootClass;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;
import soot.jimple.toolkits.callgraph.EdgePredicate;

import java.util.Collections;
import java.util.Iterator;

class AndroidAnyPathFinder extends SootCallGraphAnyPathFinder {
    public AndroidAnyPathFinder(CallGraph graph, MethodOrMethodContext entryMethod,
                                EdgePredicate edgePredicate) {
        super(graph, entryMethod, edgePredicate);
    }

    public AndroidAnyPathFinder(CallGraph graph, Iterator<MethodOrMethodContext> entryMethods,
                                EdgePredicate edgePredicate) {
        super(graph, entryMethods, edgePredicate);
    }

    @Override
    protected Iterator<Edge> computeChildren(Edge edge) {
        // Filter the traversal to only application classes.
        SootClass currentClass = edge.getTgt().method().getDeclaringClass();

        if (!currentClass.isApplicationClass()
                || currentClass.getName().startsWith("android.support.v")) {
            return Collections.<Edge>emptyList().iterator();
        }

        return super.computeChildren(edge);
    }
}
