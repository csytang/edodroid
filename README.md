# EdoDroid

EdoDroid is a static analysis tool which built on the top of IC3 for Android

# Build
## Build of IC3 and Dare
As [IC3](http://siis.cse.psu.edu/ic3/) requires [Dare](http://siis.cse.psu.edu/dare/installation.html) to create the retargeted app of the original app.
As the project is built with a Mac OS, therefore the Dare we use is also the Mac version. If you build the tool with Linux, please replace the Dare with its Linux version.

# Usage

## Run IC3
[IC3](http://siis.cse.psu.edu/ic3/) is an inter-component communication (ICC) analysis tool for Android. Edo Droid
uses IC3 as the kernal to perform inter-component analysis.

### Init Database
Initialize the database which will be used for IC3 with name `cc`. The user name and password used for this must be same as the ones provided in the `cc.properties` file. 
Once the database has been created, from the mysql command prompt you can do the following:
```bash
mysql> use cc
mysql> source path_to_schema_file
```

Note that the latest version of MySQL has a stronger prevent which makes the IC3 cannot connect the DB.
Therefore, we use an archived version (v5.7). Also, other versions may work, but not tested.


### Launch IC3
To launch IC3, you use the script defined in `script` folder with following commands:
```sh
# Run IC3 for a given apk
./runIC3.sh $path_of_apk
```

After running the IC3, it will direct the result to the database named `cc`.
Inside `cc`, users can check the database tables like the figure below.
![ICC-Tables](img/icc-table.png)

### Launch EdoDroid
Now, we are ready to launch edodroid. 
Note that, EdoDroid requires two inputs: `apkfilepath` and `Android-platform-jars`.